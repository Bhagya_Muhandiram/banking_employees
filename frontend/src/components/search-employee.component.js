import React, { Component } from "react";
import { connect } from "react-redux";
import {  fetchBranches,setCurrentSelectedValues} from "../actions/branchesAction";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import axios from 'axios'
import EmployeeService from '../services/employee.service'

import Navbar from 'react-bootstrap/Navbar';
import Nav from "react-bootstrap/Nav";
import FormControl from 'react-bootstrap/FormControl'


class SearchEmployee extends Component {
  constructor(props) {
    super(props);
    
   
    this.handleChange=this.handleChange.bind(this)
    this.searchEmployee=this.searchEmployee.bind(this)

    this.state = {
       
        index: 0,
        direction: null,
        check_in_date: new Date(),
        check_out_date: new Date(),
        location:'',
        hotels: [],
    };
  }

  
  componentWillMount() {
    //this.props.fetchEmployees();
    this.props.fetchBranches();
    EmployeeService.getSearchedEmployees().then(response => {
      this.setState({
        hotels: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
    
    axios.get('/employees/:branch_name', {
      params: {
        branch_name: 'Kandy'
      }
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
    
  }
  
  
handleChange(e){
  this.setState({ [e.target.key]: e.target.value });
  console.log(this.props.branches.branch_id,'mmmm')
}
  
searchEmployee(e){
  e.preventDefault();
  alert('nnnnn');

  //const branch = this.props.
}
  render() {
    const branchItems=this.props.branches.map(branch=>(
      <option value={branch.branch_id} key={branch.branch_id} >{branch.branch_name}</option>
    
    ))
    return (

      <React.Fragment>
        <Container>
            <Navbar bg="dark" variant="dark" fixed="top">
        <Navbar.Brand href="#home">EMPLOYEES</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link href="#home">Home</Nav.Link>
          <Nav.Link href="#features">Features</Nav.Link>
          
        </Nav>
        <Form inline>
          {/* <FormControl type="text" placeholder="Search" className="mr-sm-2" /> */}
          <Form.Control as="select"  className="mr-sm-2" custom onChange={this.handleChange}>
                                      {branchItems}
                                      </Form.Control>
          <Button variant="primary" onClick={this.searchEmployee}>Search</Button>
        </Form>
      </Navbar>
  <br />
  
   
                          {/* <Row>
                              <Col>
                              <Form.Group controlId="exampleForm.SelectCustom" style={{margin:'2%'}}>
                                      <Form.Label as='h7'></Form.Label>
                                      <Form.Control as="select"  size="sm" custom onChange={this.handleChange}>
                                      {branchItems}
                                      </Form.Control>
                                  </Form.Group>
                              </Col>
                            
                          </Row> */}
                             
                          
                         
            </Container>
      </React.Fragment>
          
    );
  }  
}


const mapStateToProps = state => ({
  branches:state.branches.branches
});

export default connect(mapStateToProps, {fetchBranches,setCurrentSelectedValues})(SearchEmployee)
