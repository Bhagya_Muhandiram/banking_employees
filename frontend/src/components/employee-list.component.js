import React, { Component } from "react";
import Table from 'react-bootstrap/Table';
import { connect } from "react-redux";
import { fetchEmployees } from "../actions/employeeAction";
import {  fetchBranches} from "../actions/branchesAction";
import Image from 'react-bootstrap/Image'
import SearchComponent from './search-employee.component'


class EmployeeList extends Component {
  
  componentWillMount() {
    this.props.fetchEmployees();

  }
  render() {
    const employeeItems=this.props.employees.map(employee=>(
      <tr style={{textAlign:'center'}} key={employee.emp_id }>
              <td><Image height='100vh' style={{borderRadius:'100px'}} src={employee.emp_photo} /></td>
             <td>{employee.emp_name}</td>
             <td>{employee.emp_email}</td>
             <td>{employee.branch_name}</td>
             <td>{employee.bank_name}</td>
          </tr>
    ))

    
    return (
      <React.Fragment>
        <div style={{margin:'10%'}}>
        <SearchComponent/>
            <Table striped bordered hover >
              <thead style={{fontWeight:'bold',textAlign:'center'}}>
              <tr>
                <th>Employee</th>
                <th>Name</th>
                <th>Email Address</th>
                <th>Branch Name</th>
                <th>Bank Name</th>
              </tr>
              </thead>
            <tbody>
              {employeeItems}
            </tbody>
            </Table>
        </div>
       


      </React.Fragment>
          
             
    );
  }  
}


const mapStateToProps = state => ({
  employees:state.employees.employees,
  //branches:state.branches.branches
});

export default connect(mapStateToProps, {fetchEmployees,fetchBranches})(EmployeeList)
