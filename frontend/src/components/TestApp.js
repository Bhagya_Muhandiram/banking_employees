import React, { Component } from "react";
import Table from 'react-bootstrap/Table';
import EmployeeDataService from "../services/employee.service";
import Dropdown from 'react-bootstrap/Dropdown'


class EmployeeList extends Component {
  constructor(props) {
    super(props);
    
    this.getEmployees = this.getEmployees.bind(this);
    
    this.state = {
      employees: [],
      branches:[],
      
    };
  }

  componentDidMount() {
    this.getEmployees();
    this.getBranches();
  }

  //Render Employee data 
  renderTableData() {
    return this.state.employees.map((employee, index) => {
       const { emp_id ,emp_name,emp_email ,emp_address,bank_name,branch_name} = employee 
       return (
          <tr key={emp_id }>
             <td>{emp_name}</td>
             <td>{emp_email}</td>
             <td>{emp_address}</td>
             <td>{branch_name}</td>
             <td>{bank_name}</td>
          </tr>
       )
    })
 }

 renderDropDown() {
  return this.state.branches.map((branch, index) => {
     const { bank_id,branch_name} = branch 
     return (
     <Dropdown.Item key={bank_id}>{branch_name}</Dropdown.Item>
     )
  })
}

 //Retrieve all Employee data
  getEmployees() {
    EmployeeDataService.getAll()
      .then(response => {
        this.setState({
          employees: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

//Retrieve all bannk branch details
  getBranches() {
    EmployeeDataService.getAllBranches()
      .then(response => {
        this.setState({
          branches: response.data
        });
        console.log(response.data,'braaanvh');
      })
      .catch(e => {
        console.log(e);
      });
  }
test(){
  alert('bhaghguy')
}
  
  
  render() {
    const employeeItems=this.state.employees.map(employee=>(
      <div key={employee.emp_id}>
        <h2>{employee.emp_name}</h2>
      </div>
    ))
    return (
      <div className=''>
        <Table striped bordered hover >
      <thead>
        <tr>
          <th>Employee Name</th>
          <th>Email Address</th>
          <th>Image</th>
          <th>Branch Name</th>
          <th>Bank Name</th>
        </tr>
      </thead>
      <tbody>
          {this.renderTableData()}
      </tbody>
</Table>
      <Dropdown>
        <Dropdown.Toggle variant="success" id="dropdown-basic">
          Dropdown Button
        </Dropdown.Toggle>
        <Dropdown.Menu>
        {this.state.branches.map((branch, index) => {
     const { bank_id,branch_name} = branch 
     return (
     <Dropdown.Item key={bank_id} onClick={this.test}>{branch_name}</Dropdown.Item>
     )
  })  }
        </Dropdown.Menu>
      </Dropdown>
      <div>
        <h1>Employees</h1>
        {employeeItems}
      </div>
      </div>
    );
  }
}
export default EmployeeList