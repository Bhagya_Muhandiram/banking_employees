import React from "react";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Provider } from "react-redux";
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import EmployeeList from "./components/employee-list.component";
import store from "./store.js";

function App() {
  return (
    <Provider store={store}>
      <Router>
          <div className="App">
  
            <Container>
              <Row>
                <Col md={12}>
                  <div className="wrapper">
                    <Switch>
                      <Route exact path='/' component={EmployeeList} />
                      <Route path="/employee-list" component={EmployeeList} />
                    </Switch>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </Router> 
    </Provider>
  
  );
}

export default App;