import {  FETCH_EMPLOYEES} from "./types"; 

 export const fetchEmployees = () => dispatch => {
     
     fetch('http://localhost:5000/employees')
     .then(res => res.json())
     .then(employees => 
        dispatch({
            type: FETCH_EMPLOYEES,
            payload: employees
        })
        );
    
 }

 
