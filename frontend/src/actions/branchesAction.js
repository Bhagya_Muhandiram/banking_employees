import {FETCH_BRANCHES,SEARCH_EMPLOYEES,SET_DROPDOWN_VALUES} from "./types"; 


 export const fetchBranches = () => dispatch => {
     
    fetch('http://localhost:5000/branch')
    .then(res => res.json())
    .then(branches => 
       dispatch({
           type: FETCH_BRANCHES,
           payload: branches
       })
       );
   
}

export const setCurrentSelectedValues = (e, data) =>{
    return{ type: SET_DROPDOWN_VALUES, payload: data.value}
}

export const searchEmployee = ()=> dispatch =>{
    fetch('http://localhost:5000/branch')
    .then(res => res.json())
    .then(searchedData => 
       dispatch({
           type: SEARCH_EMPLOYEES,
           payload: searchedData
       })
       );

}

