import http from "../http-common";

class EmployeeDataService {
  getAll() {
    return http.get("/employees");
  }

  getAllBranches(){
    return http.get("/branch")
  }

  getSearchedEmployees(){
    return http.get("/employees/Kandy")
  }
  
}

export default new EmployeeDataService();