import { combineReducers } from "redux";
import employeeReducer from './employeeReducer';
import branchReducer from './branchReducer'


export default combineReducers({
    employees : employeeReducer,
    branches :branchReducer

}) 