import {FETCH_BRANCHES,SEARCH_EMPLOYEES} from "../actions/types"; 

const initialState = {
    branches:[]
}

export default function(state = initialState, action){
    switch (action.type) {
        case FETCH_BRANCHES:
            return {
                ...state,
                branches: action.payload
            };
            case SEARCH_EMPLOYEES:
            return {
                ...state,
                searchedData: action.payload
            };
        default:
            return state;
    }
}

