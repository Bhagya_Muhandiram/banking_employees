import {  FETCH_EMPLOYEES,FETCH_BRANCHES} from "../actions/types"; 

const initialState = {
    employees:[],
    branches:[]
}

export default function(state = initialState, action){
    switch (action.type) {
        case FETCH_EMPLOYEES:
            return {
                ...state,
                employees: action.payload
            };
        default:
            return state;
    }
}