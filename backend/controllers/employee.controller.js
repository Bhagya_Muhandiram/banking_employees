const Employee = require("../models/employee.model.js");

// Create and Save a new Employee
exports.create = (req, res) => {
  
    // Validate request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    // Create an Employee
    const employee = new Employee({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        address: req.body.address,
        contact_number:req.body.contact_number,
        email:req.body.email
    });
  
    // Save Employee in the database
    Employee.create(employee, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the employee."
        });
      else res.send(data);
    });
  };

// Retrieve all Employees from the database.
exports.findAll = (req, res) => {
  Employee.getAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving employees."
        });
      else res.send(data);
    });
  };


// Find Employees with a branch_name
exports.findEmployee = (req, res) => {
  Employee.getEmployeeByBranch(req.params.branch_name, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Customer with branch name ${req.params.branch_name}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Customer with branch name " + req.params.customerId
        });
      }
    } else res.send(data);
  });
  
};
 
  

