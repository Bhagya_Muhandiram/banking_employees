const Branch = require("../models/branch.model.js");


// Retrieve all Branch Details from the database.
exports.findAll = (req, res) => {
    Branch.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving employees."
          });
        else res.send(data);
      });
    };
    
