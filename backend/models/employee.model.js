const sql = require("./db.js")

// constructor
const Employee = function(employee) {
    //this.emp_name = employee.emp_name;
  };

  Employee.getAll = function(result) {
    const get_employee_query='SELECT  e.emp_id,e.emp_name,e.emp_email,e.emp_photo,d.branch_name,b.bank_name FROM employees e JOIN bank_branch d ON e.branch_id = d.branch_id JOIN bank b ON d.bank_id=b.bank_id';
    sql.query(get_employee_query, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log("employee: ", res);
      result(null, res);
    });
  };

  //Return employees with the given Branch Name
Employee.getEmployeeByBranch = (branch_name, result) => {
  sql.query(`SELECT  e.emp_id,e.emp_name,e.emp_email,e.emp_photo,d.branch_name,b.bank_name FROM employees e JOIN bank_branch d ON e.branch_id = d.branch_id JOIN bank b ON d.bank_id=b.bank_id where  d.branch_name='${branch_name}'`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found employee: ", res[0]);
      result(null, res);
      return;
    }

    // not found Employee with the branch name
    result({ kind: "not_found" }, null);
  });
};

module.exports = Employee;