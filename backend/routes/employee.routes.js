module.exports = app => {
  const employee = require("../controllers/employee.controller.js");

  // Create a new Employee
  app.post("/employees", employee.create);

  // Retrieve all Employees
  app.get("/employees", employee.findAll);

  //Get employees for a given Branch Name
  //app.get('/employees/:branchId',employee.findEmployee)
    

  // Retrieve a single Employee with branch_name
  app.get("/employees/:branch_name", employee.findEmployee);


  

};